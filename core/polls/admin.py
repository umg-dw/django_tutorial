from django.contrib import admin
from .models import Question,Choice

# Register your models here.
@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display=['question_text']
    list_filter=[]
    list_editable=[]
    list_per_page=15
    search_fields=['question_text']

@admin.register(Choice)
class ChoiceAdmin(admin.ModelAdmin):
    list_display=['choice_text','question','votes']
    list_filter=[]
    list_editable=[]
    list_per_page=15
    search_fields=['choice_text']