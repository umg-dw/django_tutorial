from django.shortcuts import render
from django.views.generic import TemplateView
from .models import Question, Choice

class InicioView(TemplateView):
    template_name='index.html'

    def get(self, request):
        choices=Choice.objects.all()
        questions=Question.objects.all()
        return render(request,self.template_name,{'questions':questions,'choices':choices})