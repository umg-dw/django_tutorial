from django.urls import path

from .views import ListaView

urlpatterns = [
    path("lista", ListaView.index, name="index"),
]