from django.db import models
from django.utils import timezone
from datetime import datetime

class Question(models.Model):
    question_text = models.CharField(db_column='QUESTION_TEXT',verbose_name='Pregunta',max_length=200)
    pub_date = models.DateTimeField(db_column='PUB_DATE',verbose_name='Publicado el', default=timezone.now)

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)

    class Meta:
        verbose_name='Pregunta'
        verbose_name_plural='Preguntas'
        db_table='QUESTION'


class Choice(models.Model):
    question = models.ForeignKey(Question, verbose_name='Pregunta', on_delete=models.CASCADE)
    choice_text = models.CharField(verbose_name='Opción',max_length=200)
    votes = models.BigIntegerField(default=0)
    
    
    def __str__(self):
        return self.choice_text

    class Meta:
        verbose_name='Opción'
        verbose_name_plural='Opciones'
        db_table='CHOICE'
